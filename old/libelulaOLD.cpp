#include <iostream>
#include <fstream>
#include <string>
using namespace std;
///////////////////////////Arreglos de Palabras reservadas///////////////////////////////
///Palabras Reservadas
string palReser[] = {"funcion", 
	"entero", "text", "double", "bool", 
	"Para", "leer", "imp",  "Mientras", 
	"nulo", "Si", "Sino"};
///////////////////////////Separadores///////////////////////////////

///Separador de identificadores y palabras reservadas
string SPalabrasReservadas[] ={" ","=",";","(",")","<","+"};
///Separador de numeros
string Snumeros[] = {" ", ";","+", "-", "*", "/", "<", ">",")","!"};

///Separadores ; 
string Scaracteres[] = {" ","\n"};

///Separadores + - * /
string SAritmeticos[] = {" "};

///Separadores < > !
string SLogicos[] = {" "};

///Separadores (
string SParetAbierto[] = {" ","!",")"};

///Separadores )
string SParetCerrado[] = {" ",";","{","+", "-", "*", "/", "<", ">"};

///Separadores {
string SCortAbierto[] = {" ","\n"};

///Separadores {
string SCortCerrado[] = {" ","\n"};

///Separadores = ==
string SIgual[] = {" ","("};
////////////////////Metodos para buscar en los Separadores/////////////////////////
/// Metodo para buscar separadores de las palabras reservadas
bool buscarSPalabrasReservadas(string c){ 
	
	int limite= sizeof(SPalabrasReservadas)/sizeof(*SPalabrasReservadas);
	for(int i=0; i<limite; i++){
		if(c==SPalabrasReservadas[i]){
			
			return true;
		}
	}
	return false;
}
/// Metodo para buscar separadores de los numeros
bool buscarSnumeros(string c){ 
	
	int limite= sizeof(Snumeros)/sizeof(*Snumeros);
	for(int i=0; i<limite; i++){
		if(c==Snumeros[i]){
			
			return true;
		}
	}
	return false;
}
/// Metodo para buscar separadores para el punto y coma
bool buscarScaracteres(string token){
	int limite= sizeof(Scaracteres)/sizeof(*Scaracteres);
	for(int i=0; i<limite; i++){
		if(token==Scaracteres[i]){
			return true;
		}
	}
	return false;
}
/// Metodo para buscar separadores para los operadores aritmeticos
bool buscarSAritmeticos(string c){ 
	
	int limite= sizeof(SAritmeticos)/sizeof(*SAritmeticos);
	for(int i=0; i<limite; i++){
		if(c==SAritmeticos[i]){
			
			return true;
		}
	}
	return false;
}
/// Metodo para buscar separadores para los operadores logicos
bool buscarSLogicos(string c){ 
	
	int limite= sizeof(SLogicos)/sizeof(*SLogicos);
	for(int i=0; i<limite; i++){
		if(c==SLogicos[i]){
			
			return true;
		}
	}
	return false;
}
///Metodos para buscar separadores de Parentisis
bool buscarSParetAbierto(string c){ 
	
	int limite= sizeof(SParetAbierto)/sizeof(*SParetAbierto);
	for(int i=0; i<limite; i++){
		if(c==SParetAbierto[i]){
			
			return true;
		}
	}
	return false;
}
bool buscarSParetCerrado(string c){ 
	
	int limite= sizeof(SParetCerrado)/sizeof(*SParetCerrado);
	for(int i=0; i<limite; i++){
		if(c==SParetCerrado[i]){
			
			return true;
		}
	}
	return false;
}
///Metodos para buscar separadores de Corchetes
bool buscarSCortAbierto(string c){ 
	
	int limite= sizeof(SCortAbierto)/sizeof(*SCortAbierto);
	for(int i=0; i<limite; i++){
		if(c==SCortAbierto[i]){
			
			return true;
		}
	}
	return false;
}
bool buscarSCortCerrado(string c){ 
	
	int limite= sizeof(SCortCerrado)/sizeof(*SCortCerrado);
	for(int i=0; i<limite; i++){
		if(c==SCortCerrado[i]){
			
			return true;
		}
	}
	return false;
}
///Metodos para buscar separadores de Igual y asignación
bool buscarSIgual(string token){
	int limite= sizeof(SIgual)/sizeof(*SIgual);
	for(int i=0; i<limite; i++){
		if(token==SIgual[i]){
			return true;
		}
	}
	return false;
}


bool buscarPalabraReservada(string token){
	int limite= sizeof(palReser)/sizeof(*palReser);
	for(int i=0; i<limite; i++){
		if(token==palReser[i]){
			return true;
		}
		
	}
	
	return false;
}


int main(int argc, char *argv[]) {
	
	ifstream ficheroEntrada;
	///RUTA del archivo
	ficheroEntrada.open("sumarNum.ax");
	
	///Declaración de Varibles 
	char c;
	int estado=0;
	string token;
	string aux;
	bool error;
	
	///Leyendo el archivo
	while(!ficheroEntrada.eof() && error==false){
		///Tomando los caracteres
		ficheroEntrada.get(c);
		
		
		switch(estado){
		case 0:
			if(isalpha(c)){
				estado= 1;
				token+=c;
			}
			else if(isdigit(c)){
				estado=2;
				token+=c;
			}
			
			else if(c=='+'){
				estado=5;
				token+=c;
			}
			else if(c=='-'){
				estado=6;
				token+=c;
			}
			else if(c=='*'){
				estado=7;
				token+=c;
			}
			else if(c=='/'){
				estado=8;
				token+=c;
			}
			else if(c=='>'){
				estado=9;
				token+=c;
			}
			else if(c=='<'){
				estado=10;
				token+=c;
			}
			else if(c==';'){
				estado=11;
				token+=c;
			}
			else if(c=='='){
				estado=12;
				token+=c;
			}
			else if(c=='!'){
				estado=14;
				token+=c;
			}
			else if(c=='('){
				estado=15;
				token+=c;
			}
			else if(c==')'){
				estado=16;
				token+=c;
			}
			else if(c=='{'){
				estado=17;
				token+=c;
			}
			else if(c=='}'){
				estado=18;
				token+=c;
			}
			
			break;
			case 1:
				///Caso de identificadores y palabras reservadas
				if(isalpha(c)){
					estado= 1;
					token+=c;
				}else if(isdigit(c)){
					estado=1; 
					token+=c;
				}else{
					aux=c;
					if(buscarSPalabrasReservadas(aux)){
						ficheroEntrada.unget();
						if (buscarPalabraReservada(token)){
							cout<<"["<<token<<"] reservada"<<endl;
						}else{
							cout<<"["<<token<<"] identificador"<<endl;
						}
						token="";
						estado =0;
						
					}else{
						error=true;
						cout<<"["<<token<<c<<"] Error: falta ;"<<endl;
					}
				}
				
				break;
			case 2:
				///Caso del numero
				
				if(isdigit(c)){
					estado=2;
					token+=c;
				}else if(c==','){
					estado=3;
					token+=c;
				}else {
					aux=c;
					if(buscarSnumeros(aux)){
						ficheroEntrada.unget();
						cout<<"["<<token<<"] numero "<<endl;
						
					}else if(c=='='){ 
						error=true;
						cout<<"["<<token<<"] Error: Aun numero no se le puede asignar un valor"<<endl;
					}else{
						error=true;
						cout<<"["<<token<<c<<"] Error: falta ;"<<endl;
					}
					token=""; 
					estado=0;
				}			
				
				break;
			case 3:
				///Caso de la , de un numero decimal
				if(isdigit(c)){
					token+=c;
					estado=4;
				}else{
					error=true;
					cout<<"["<<c<<"] Error: 0001"<<endl;
				}
				break;
			case 4:
				///Caso del numero decimal
				if(isdigit(c)){
					token+=c;
					estado=4;
				}else {
					aux=c;
					if(buscarSnumeros(aux)){
						ficheroEntrada.unget();
						cout<<"["<<token<<"] es numero "<<endl;
						
					}
					token=""; 
					estado=0;
				}
				break;
			case 5:
				///Caso del caracter "+"
				
				aux=c;
				if(buscarSAritmeticos(aux)||isalpha(c) || isdigit(c) ){
					cout<<"["<<token<<"] es un operador aritmetico +"<<endl;
					
				}
				token="";
				estado=0;
				ficheroEntrada.unget();
				break;
			case 6:
				///Caso del caracter "-"
				
				aux=c;
				if(buscarSAritmeticos(aux)||isalpha(c) || isdigit(c) ){
					cout<<"["<<token<<"] es un operador aritmetico -"<<endl;
					
				}
				token="";
				estado=0;
				ficheroEntrada.unget();
				break;
			case 7:
				///Caso del caracter "*"
				
				aux=c;
				if(buscarSAritmeticos(aux)||isalpha(c) || isdigit(c) ){
					cout<<"["<<token<<"] es un operador aritmetico *"<<endl;
					
				}
				token="";
				estado=0;
				ficheroEntrada.unget();
				break;
			case 8:
				///Caso del caracter "/"
				
				aux=c;
				if(buscarSAritmeticos(aux)||isalpha(c) || isdigit(c) ){
					cout<<"["<<token<<"] es un operador aritmetico /"<<endl;
					
				}
				token="";
				estado=0;
				ficheroEntrada.unget();
				break;	
			case 9:
				///Caso del caracter ">"
				
				aux=c;
				if(buscarSLogicos(aux)||isalpha(c) || isdigit(c) ){
					cout<<"["<<token<<"] es un operador logico >"<<endl;
					
				}
				token="";
				estado=0;
				ficheroEntrada.unget();
				break;
			case 10:
				///Caso del caracter "<"
				
				aux=c;
				if(buscarSLogicos(aux)||isalpha(c) || isdigit(c) ){
					cout<<"["<<token<<"] es un operador logico <"<<endl;
					
				}
				token="";
				estado=0;
				ficheroEntrada.unget();
				break;
			case 11:
				///Caso del caracter ";"
				
				aux=c;
				if(buscarScaracteres(aux)||isalpha(c) || isdigit(c) ){
					cout<<"["<<token<<"] es un separador ;"<<endl;
					
				}
				token="";
				estado=0;
				ficheroEntrada.unget();
				//			else{
				//				cout<<"["<<aux<<"] ESTO"<<endl;
				//			}
				break;
				case 12:
					///Caso del caracter "="
					if(c=='='){
						estado=13;
						token+=c;
					}else{
						aux=c;
						if(buscarSIgual(aux)||isalpha(c) || isdigit(c) ){
							cout<<"["<<token<<"] es un operador de asignación ="<<endl;
							
						}
						token="";
						estado=0;
						ficheroEntrada.unget();
					}
					
					break;
				case 13:
					///Caso del caracter "=="
					aux=c;
					if(buscarSIgual(aux)||isalpha(c) || isdigit(c) ){
						cout<<"["<<token<<"] es un operador logico =="<<endl;
						
					}
					token="";
					estado=0;
					ficheroEntrada.unget();
					break;
					
				case 14:
					///Caso del caracter "!"
					
					aux=c;
					if(buscarSLogicos(aux)||isalpha(c) || isdigit(c) ){
						cout<<"["<<token<<"] es un operador logico !"<<endl;
						
					}
					token="";
					estado=0;
					ficheroEntrada.unget();
					break;
				case 15:
					///Caso del caracter "("
					
					aux=c;
					if(buscarSParetAbierto(aux)||isalpha(c) || isdigit(c) ){
						cout<<"["<<token<<"] es un separador ("<<endl;
						
					}
					token="";
					estado=0;
					ficheroEntrada.unget();
					break;
				case 16:
					///Caso del caracter ")"
					
					aux=c;
					if(buscarSParetCerrado(aux)||isalpha(c) || isdigit(c) ){
						cout<<"["<<token<<"] es un separador )"<<endl;
						
					}else{
						error=true;
						cout<<"["<<token<<c<<"] Error: falta ;"<<endl;
					}
					token="";
					estado=0;
					ficheroEntrada.unget();
					break;
				case 17:
					///Caso del caracter "{"
					
					aux=c;
					if(buscarSCortAbierto(aux)||isalpha(c) || isdigit(c) ){
						cout<<"["<<token<<"] es un separador {"<<endl;
						
					}
					token="";
					estado=0;
					ficheroEntrada.unget();
					break;
					case 18:
						///Caso del caracter "}"
						
						aux=c;
						if(buscarSCortCerrado(aux)||isalpha(c) || isdigit(c) ){
							cout<<"["<<token<<"] es un separador }"<<endl;
							
						}
						token="";
						estado=0;
						ficheroEntrada.unget();
						break;
		}
		
		
	}
	ficheroEntrada.close();
	return 0;
}
