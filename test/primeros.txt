P(l_sentencias)             →   { int, id, entrada, imp }
P(sentencias)               →   { int, id, entrada, imp, if }
P(l_nombre)                 →   { id }
P(if)                       →   { if }
P(condicion)                →   { (, id }
P(expresion)                →   { (, id }
P(terminos)                 →   { (, id }
P(factor)                   →   { (, id }
P(condicion)                →   { (, id }
P(l_logical_relac)          →   { <, > }