#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;
///Palabras Reservadas
string palabras_reservadas[] = {"funcion", 
	"entero", "text", "double", "bool", 
	"Para", "leer", "imp",  "Mientras", 
	"nulo", "Si", "Sino", "Inicio", "Fin"};

//char operatorArithmetics[] = {'+','-','*','/','%'};
string separadorPalabrasReservadas[] ={" ","=",";","(",")","<","+","\n"};
string separadorNumeros[] = {" ", ";","+", "-", "*", "/", "<", ">",")","!","%"};
string separadorCaracteres[] = {" ","\n"};
string separadoresAritmeticos[] = {" "};
string separadoresLogicos[] = {" "};
///Separadores (
string separadorParentesisAbierto[] = {" ","!",")"};
///Separadores )
string separadorParentesisCerrado[] = {" ",";","{","+", "-", "*", "/", "<", ">"};
///Separadores {
string separadorLlaveAbierta[] = {" ","\n"};
///Separadores {
string separadorLlaveCerrada[] = {" ","\n"};
///Separadores = ==
string separadorIgual[] = {" ","("};
/// Metodo para buscar separadores de las palabras reservadas
bool buscarseparadorPalabrasReservadas(string c){ 
	int limite= sizeof(separadorPalabrasReservadas)/sizeof(*separadorPalabrasReservadas);
	for(int i=0; i<limite; i++){
		if(c==separadorPalabrasReservadas[i]){
			return true;
		}
	}
	return false;
}
/// Metodo para buscar separadores de los numeros
bool buscarseparadorNumeros(string c){ 
	int limite= sizeof(separadorNumeros)/sizeof(*separadorNumeros);
	for(int i=0; i<limite; i++){
		if(c==separadorNumeros[i]){
			return true;
		}
	}
	return false;
}
/// Metodo para buscar separadores para el punto y coma
bool buscarseparadorCaracteres(string token){
	int limite= sizeof(separadorCaracteres)/sizeof(*separadorCaracteres);
	for(int i=0; i<limite; i++){
		if(token==separadorCaracteres[i]){
			return true;
		}
	}
	return false;
}
/// Metodo para buscar separadores para los operadores aritmeticos
bool buscarseparadoresAritmeticos(string c){ 
	int limite= sizeof(separadoresAritmeticos)/sizeof(*separadoresAritmeticos);
	for(int i=0; i<limite; i++){
		if(c==separadoresAritmeticos[i]){
			return true;
		}
	}
	return false;
}
/// Metodo para buscar se paradores para los operadores logicos
bool buscarseparadoresLogicos(string c){ 
	int limite= sizeof(separadoresLogicos)/sizeof(*separadoresLogicos);
	for(int i=0; i<limite; i++){
		if(c==separadoresLogicos[i]){
			return true;
		}
	}
	return false;
}
///Metodos para buscar separadores de Parentisis
bool buscarseparadorParentesisAbierto(string c){ 
	int limite= sizeof(separadorParentesisAbierto)/sizeof(*separadorParentesisAbierto);
	for(int i=0; i<limite; i++){
		if(c==separadorParentesisAbierto[i]){
			return true;
		}
	}
	return false;
}
bool buscarseparadorParentesisCerrado(string c){ 
	int limite= sizeof(separadorParentesisCerrado)/sizeof(*separadorParentesisCerrado);
	for(int i=0; i<limite; i++){
		if(c==separadorParentesisCerrado[i]){
			return true;
		}
	}
	return false;
}
///Metodos para buscar separadores de Corchetes
bool buscarseparadorLlaveAbierta(string c){ 
	int limite= sizeof(separadorLlaveAbierta)/sizeof(*separadorLlaveAbierta);
	for(int i=0; i<limite; i++){
		if(c==separadorLlaveAbierta[i]){
			return true;
		}
	}
	return false;
}
bool buscarseparadorLlaveCerrada(string c){ 
	int limite= sizeof(separadorLlaveCerrada)/sizeof(*separadorLlaveCerrada);
	for(int i=0; i<limite; i++){
		if(c==separadorLlaveCerrada[i]){
			return true;
		}
	}
	return false;
}
///Metodos para buscar separadores de Igual y asignaci�n
bool buscarseparadorIgual(string token){
	int limite= sizeof(separadorIgual)/sizeof(*separadorIgual);
	for(int i=0; i<limite; i++){
		if(token==separadorIgual[i]){
			return true;
		}
	}
	return false;
}
bool buscarPalabraReservada(string token){
	int limite= sizeof(palabras_reservadas)/sizeof(*palabras_reservadas);
	for(int i=0; i<limite; i++){
		if(token==palabras_reservadas[i]){
			return true;
		}
	}
	return false;
}

// Metodo que retorna un array con los token del analizador lexico
std::vector<string> libelula() {


	ifstream ficheroEntrada;

	///RUTA del archivo
	ficheroEntrada.open("sumarNum.ax");
	///Declaración de Varibles 
	char c;
	int estado = 0;
	string token;
	string aux;
	bool error;
	std::vector<string> lista;
	///Leyendo el archivo
	while(!ficheroEntrada.eof() && error==false){
		///Tomando los caracteres
		ficheroEntrada.get(c);
		switch(estado){
		case 0:
			if(isalpha(c)){
				estado= 1;
				token+=c;
			}
			else if(isdigit(c)){
				estado=2;
				token+=c;
			}
			else if(c=='+'){
				estado=5;
				token+=c;
			}
			else if(c=='-'){
				estado=6;
				token+=c;
			}
			else if(c=='*'){
				estado=7;
				token+=c;
			}
			else if(c=='/'){
				estado=8;
				token+=c;
			}
			else if(c=='>'){
				estado=9;
				token+=c;
			}
			else if(c=='<'){
				estado=10;
				token+=c;
			}
			else if(c==';'){
				estado=11;
				token+=c;
			}
			else if(c=='='){
				estado=12;
				token+=c;
			}
			else if(c=='!'){
				estado=14;
				token+=c;
			}
			else if(c=='('){
				estado=15;
				token+=c;
			}
			else if(c==')'){
				estado=16;
				token+=c;
			}
			else if(c=='{'){
				estado=17;
				token+=c;
			}
			else if(c=='}'){
				estado=18;
				token+=c;
			}

			break;
		case 1:
			///Caso de identificadores y palabras reservadas
			if(isalpha(c)){
				estado= 1;
				token+=c;
			}else if(isdigit(c)){
				estado=1; 
				token+=c;
			}else{
				aux=c;
				
				if(buscarseparadorPalabrasReservadas(aux)){
					ficheroEntrada.unget();
						if (buscarPalabraReservada(token)){
							cout<<"["<<token<<"]  Reservada"<<endl;
							lista.push_back(token);
						
						}else{
							cout<<"["<<token<<"]  Identificador"<<endl;
							lista.push_back("id");
						
						}
					token = "";
					estado = 0;
				}else{
					error=true;
					cout<<"["<<token<<c<<"] Error: falta ;"<<endl;
				}
			}
			break;
		case 2:
			///Caso del numero
			if(isdigit(c)){
				estado=2;
				token+=c;
			}else if(c==','){
				estado=3;
				token+=c;
			}else {
				aux=c;
				if(buscarseparadorNumeros(aux)){
					ficheroEntrada.unget();
					cout<<"["<<token<<"]  Numero"<<endl;
					lista.push_back("id");
				}else if(c=='='){ 
					error=true;
					cout<<"["<<token<<"] Error: Aun numero no se le puede asignar un valor"<<endl;
				}else{
					error=true;
					cout<<"["<<token<<c<<"] Error: falta ;"<<endl;

				}
				token=""; 
				estado=0;
			}			
			break;
		case 3:
			///Caso de la , de un numero decimal
			if(isdigit(c)){
				token+=c;
				estado=4;
			}else{
				error=true;
				cout<<"["<<c<<"] Error: 0001"<<endl;
			
			}
			break;
		case 4:
			///Caso del numero decimal
			if(isdigit(c)){
				token+=c;
				estado=4;
			}else {
				aux=c;
				if(buscarseparadorNumeros(aux)){
					ficheroEntrada.unget();
					cout<<"["<<token<<"]  Numero"<<endl;
					lista.push_back("id");
				}
				token=""; 
				estado=0;
			}
			break;
		case 5:
			///Caso del caracter "+"
			aux=c;
			if(buscarseparadoresAritmeticos(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"]  Aritmetico"<<endl;
				lista.push_back(token);
				
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
		case 6:
			///Caso del caracter "-"
			aux=c;
			if(buscarseparadoresAritmeticos(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"]  Aritmetico"<<endl;
				lista.push_back(token);
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
			
		case 7:
			///Caso del caracter "*"
			
			aux=c;
			if(buscarseparadoresAritmeticos(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"]  Aritmetico"<<endl;
				lista.push_back(token);
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
		case 8:
			///Caso del caracter "/"
			
			aux=c;
			if(buscarseparadoresAritmeticos(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"]  Aritmetico"<<endl;
				lista.push_back(token);
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;	
		case 9:
			///Caso del caracter ">"
			
			aux=c;
			if(buscarseparadoresLogicos(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"]  Logico"<<endl;
			
				lista.push_back(token);
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
		case 10:
			///Caso del caracter "<"
			aux=c;
			if(buscarseparadoresLogicos(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"]  Logico"<<endl;
				lista.push_back(token);
			
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
		case 11:
			aux=c;
			if(buscarseparadorCaracteres(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"]  Separador"<<endl;
				lista.push_back(token);
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
		case 12:
			///Caso del caracter "="
			if(c=='='){
				estado=13;
				token+=c;
			}else{
				aux=c;
				if(buscarseparadorIgual(aux)||isalpha(c) || isdigit(c) ){
					cout<<"["<<token<<"]  Asignacion"<<endl;
					lista.push_back(token);		
				}
				token="";
				estado=0;
				ficheroEntrada.unget();
			}

			break;
		case 13:
			///Caso del caracter "=="
			aux=c;
			if(buscarseparadorIgual(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"] es un operador logico"<<endl;
				lista.push_back(token);				
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;

		case 14:
			///Caso del caracter "!"
			aux=c;
			if(buscarseparadoresLogicos(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"] es un operador logico"<<endl;
				lista.push_back(token);				
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
		case 15:
			///Caso del caracter "("
			aux=c;
			if(buscarseparadorParentesisAbierto(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"] Separador"<<endl;
				lista.push_back(token);
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
		case 16:
			///Caso del caracter ")"

			aux=c;
			if(buscarseparadorParentesisCerrado(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"] Separador"<<endl;
				
				lista.push_back(token);
				
			}else{
				error=true;
				cout<<"["<<token<<"] Error: falta ;"<<endl;
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
		case 17:
			///Caso del caracter "{"
			aux=c;
			if(buscarseparadorLlaveAbierta(aux)||isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"] Separador"<<endl;
				
				lista.push_back(token);
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
		case 18:
			///Caso del caracter "}"
			aux=c;
			if(buscarseparadorLlaveCerrada(aux) || isalpha(c) || isdigit(c) ){
				cout<<"["<<token<<"] Separador"<<endl;
				
				lista.push_back(token);
			}
			token="";
			estado=0;
			ficheroEntrada.unget();
			break;
	}
	}

	ficheroEntrada.close();
	lista.push_back("$"); // Se agrega el $ al array de tokens 
	return lista;
}


///////////////////Analisis Sintactico///////////////////

struct Nodo{
	int dato;
	Nodo *siguiente;
};

// Metodo que push a pila
void push(Nodo *&pila, int n){
	Nodo *nuevoNodo = new Nodo();
	nuevoNodo->dato = n;
	nuevoNodo->siguiente = pila;
	pila = nuevoNodo; 
}
// Metodo que pop a pila
void pop(Nodo *&pila){
	Nodo *aux = pila;
	pila = aux->siguiente;
	delete aux;
}
// Metodo que muestra la pila
void show(Nodo *pila){
	Nodo *actual = new Nodo();
	actual = pila;
	while(actual != NULL){
		cout<<actual->dato<<" | ";
		actual= actual->siguiente;
	}
	cout<<endl;
}
int contador=0; // contador para metodo de recursividad
void accion(std::vector<string> codigo,  Nodo *pila, string accionHeader [17], int arrayAccion[36][17], string arrayHeadirA [10], int arrayirA[40][10] ){
   

	///  Reglas de la gramatica
	string reglas [16] = { "libelula'", "libelula", "arreglo_semtencia", "arreglo_semtencia", "sentencia", "sentencia", "sentencia", "condicion",
							"expresion", "expresion", "termino", "termino", "factor"  ,"factor", "operadores", "operadores"};
	
	// Permite terminar el metodo de recursividad
	if ( contador < codigo.size() ) {

		// Muestra el estado que esta y hacia la cabecera en ese punto
		cout<<">> Estado \t["<<pila->dato<<"]\t en ["<<codigo[contador]<<"]";
		for ( int i = 0; i < 17 ; i++ ) {
			// Compara el codigo con las cabeceras de accion
			if ( codigo[contador] == accionHeader[i] ) {

				if ( arrayAccion[pila->dato][i] > 0 ) { 
					cout<<"\tdesplazar ->\t["<<arrayAccion[pila->dato][i]<<"]"<<endl<<endl; 
					//  Agrega a la pila el valor donde se desplazara
					push(pila, arrayAccion[pila->dato][i]); 
				}else{
					//// Presenta hacia donde se reduce
					cout<<"\t reduce -> \t["<<arrayAccion[pila->dato][i]<<"]"<<endl; 

					// Regla 1
					if(arrayAccion[pila->dato][i] == -1){ 
						// Elimina Posiciones
						for(int i = 0; i < 3; i++){  
							pop(pila);  
						}
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[1] == arrayHeadirA[i]){ 
							
								cout<<"-- Regla 1 ["<<reglas[1]<<"] -> ["<<i<<"] quita 3 elementos a la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]);
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA );
							}
						}
					}

					// Regla 2
					if(arrayAccion[pila->dato][i]==-2){ 
						// Elimina Posiciones
						for(int i=0; i< 2;i++){
							pop(pila); 
						}
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[2] == arrayHeadirA[i]){ 
								cout<<"-- Regla 2 ["<<reglas[2]<<"] -> ["<<i<<"] quita 2 elementos en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]); 
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA ); 
							}
						}
					}

					// Regla 3
					if(arrayAccion[pila->dato][i]==-3){ 
						// Elimina Posiciones
						pop(pila); 
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[3] == arrayHeadirA[i]){ 
								cout<<"-- Regla 3 ["<<reglas[3]<<"] -> ["<<i<<"] quita 1 elemento en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]);
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA ); 
							}
						}
					}

					// Regla 4
					if ( arrayAccion[pila->dato][i] == -4 ) { 
						// Elimina Posiciones
						for ( int i = 0; i< 3; i++ ) {
							pop(pila); 
						}
						for ( int i = 0; i < 10; i++ ) {
							// Se verifica si la regla existe en la cabecera de IrA
							if (reglas[4] == arrayHeadirA[i] ) { 
								cout<<"-- Regla 4 ["<<reglas[4]<<"] -> ["<<i<<"] quita 3 elementos en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]); 
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA ); 
							}
						}
					}	

					// Regla 5
					if(arrayAccion[pila->dato][i]==-5){ 
						// Elimina Posiciones
						for(int i=0; i< 4;i++){
							pop(pila); 
						}
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[5] == arrayHeadirA[i]){ 
								cout<<"-- Regla 5 ["<<reglas[5]<<"] -> ["<<i<<"] quita 4 elementos en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]); 
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA ); 
							}
						}
					}	

					// Regla 6
					if( arrayAccion[pila->dato][i] == -6 ){ 
						// Elimina Posiciones
						for( int i = 0; i < 7; i++){
							pop(pila); 
						}
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[6] == arrayHeadirA[i]){ 
								cout<<"-- Regla 6 ["<<reglas[6]<<"] -> ["<<i<<"] quita 7 elementos en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]); 
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA ); 
							}
						}
					}

					// Regla 7
					if(arrayAccion[pila->dato][i]==-7){ 
						// Elimina Posiciones
						for(int i=0; i< 3;i++){
							pop(pila); 
						}
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[7] == arrayHeadirA[i]){ 
								cout<<"-- Regla 7 ["<<reglas[7]<<"] -> ["<<i<<"] quita 3 elementos en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]);  
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA ); 
							}
						}
					}	

					// Regla 8
					if(arrayAccion[pila->dato][i]==-8){ 
						// Elimina Posiciones
						for(int i=0; i< 3;i++){
							pop(pila); 
						}
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[8] == arrayHeadirA[i]){ 
								cout<<"-- Regla 8 ["<<reglas[8]<<"] -> ["<<i<<"] quita 3 elementos en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]);   
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA ); 
							}
						}
					}

					// Regla 9
					if(arrayAccion[pila->dato][i]==-9){  
						// Elimina Posiciones
						pop(pila); 
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[9] == arrayHeadirA[i]){  
								cout<<"-- Regla 9 ["<<reglas[9]<<"] -> ["<<i<<"] quita 1 elemento en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]);  
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA ); 
							}
						}
					}

					// Regla 10
					if(arrayAccion[pila->dato][i]==-10){  
						// Elimina Posiciones
						for(int i=0; i< 3;i++){
							pop(pila); 
						}
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[10] == arrayHeadirA[i]){
								cout<<"-- Regla 10 ["<<reglas[10]<<"] -> ["<<i<<"] quita 3 elementos en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]); 
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA ); 
							}
						}
					}	


					// Regla 11
					if(arrayAccion[pila->dato][i]==-11){  
						// Elimina Posiciones
						pop(pila); 
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[11] == arrayHeadirA[i]){
								cout<<"-- Regla 11 ["<<reglas[11]<<"] -> ["<<i<<"] quita 1 elemento en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]);
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA );
							}
						}
					}	

					// Regla 12
					if(arrayAccion[pila->dato][i]==-12){  
						// Elimina Posiciones
						pop(pila);
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[12] == arrayHeadirA[i]){
								cout<<"-- Regla 12 ["<<reglas[12]<<"] -> ["<<i<<"] quita 1 elemento en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]);
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA );
							}
						}
					}	

					// Regla 13
					if(arrayAccion[pila->dato][i]==-13){
						// Elimina Posiciones
						for(int i=0; i< 3;i++){
							pop(pila); 
						}
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[13] == arrayHeadirA[i]){
								cout<<"-- Regla 13 ["<<reglas[13]<<"] -> ["<<i<<"] quita 3 elementos en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]);
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA );
							}
						}
					}	

					// Regla 14
					if(arrayAccion[pila->dato][i]==-14){
						// Elimina Posiciones
						pop(pila); 
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[14] == arrayHeadirA[i]){
								cout<<"-- Regla 14 ["<<reglas[14]<<"] -> ["<<i<<"] quita 1 elemento en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]);
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA );
							}
						}
					}

					// Regla 15
					if(arrayAccion[pila->dato][i]==-15){ 
						// Elimina Posiciones
						pop(pila);
						for(int i = 0; i < 10; i++){
							// Se verifica si la regla existe en la cabecera de IrA
							if(reglas[15] == arrayHeadirA[i]){
								cout<<"-- Regla 15 ["<<reglas[15]<<"] -> ["<<i<<"] quita 1 elemento en la pila"<<endl;
								cout<<"-- Push (agrega) ["<<arrayirA[pila->dato][i]<<"] a la pila"<<endl<<endl;
								// Agrega a la pila el valor que se encuentra en esa posicion
								push(pila, arrayirA[pila->dato][i]);
								// Se llama al metdo recursivo
								accion(codigo,pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA );
							}
						}
					}

				}
			}
		}
		contador++;
		//show(pila);
		if(pila->dato== 1000){
			cout<<"______________________________________________________________"<<endl;
			cout<<"\n";
			cout<<"---------------------------"<<endl;
			cout<<"| 1000 OK,                 |\n| Gramatica correcta...    |"<<endl;
			cout<<"---------------------------"<<endl;
		}
		accion(codigo, pila,accionHeader, arrayAccion, arrayHeadirA, arrayirA ); 
		
	}
}

int main(int argc, char *argv[]) {

	std::vector<string> arregloCodigo; 

	// Declacion de Pila
	Nodo *pila = NULL; 
	push(pila, 0); 

	///  Matriz Accion
	string accionHeader [17] = { "estado", "Inicio", "Fin", "entero", "id", ";", "=", "Si", "(", ")", "{", "}", "+"  ,"*", "<", ">", "$" };
	int arrayAccion[36][17] = {  
						//S   Inc   Fin  ent  id    ;    =   Si   (    )    {     }    +    *    <    >    $ 
						{ 0,   2,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   0,   0,   0 }, 
						{ 1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   0,   0, 1000 },
						{ 2,   0,   0,   5,   6,   0,   0,   7,   0,   0,   0,   0,    0,   0,   0,   0,   0 }, 
						{ 3,   0,   8,   5,   6,   0,   0,   7,   0,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 4,   0,  -3,  -3,  -3,   0,   0,  -3,   0,   0,   0,  -3,    0,   0,   0,   0,   0 },
						{ 5,   0,   0,   0,  10,   0,   0,   0,   0,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 6,   0,   0,   0,   0,   0,  11,   0,   0,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 7,   0,   0,   0,   0,   0,   0,   0,  12,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 8,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   0,   0,  -1 },
						{ 9,   0,  -2,  -2,  -2,   0,   0,  -2,   0,   0,   0,  -2,    0,   0,   0,   0,   0 },
						{ 10,  0,   0,   0,   0,  13,   0,   0,   0,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 11,  0,   0,   0,  17,   0,   0,   0,  18,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 12,  0,   0,   0,  17,   0,   0,   0,  18,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 13,  0,  -4,  -4,  -4,   0,   0,  -4,   0,   0,   0,  -4,    0,   0,   0,   0,   0 },
						{ 14,  0,   0,   0,   0,  21,   0,   0,   0,   0,   0,   0,   22,   0,   0,   0,   0 },
						{ 15,  0,   0,   0,   0,  -9,   0,   0,   0,  -9,   0,   0,   -9,  23,  -9,  -9,   0 },
						{ 16,  0,   0,   0,   0, -11,   0,   0,   0, -11,   0,   0,  -11, -11, -11, -11,   0 },
						{ 17,  0,   0,   0,   0, -12,   0,   0,   0, -12,   0,   0,  -12, -12, -12, -12,   0 },
						{ 18,  0,   0,   0,  17,   0,   0,   0,  18,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 19,  0,   0,   0,   0,   0,   0,   0,   0,  25,   0,   0,    0,   0,   0,   0,   0 },
						{ 20,  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   22,   0,  27,  28,   0 },
						{ 21,  0,  -5,  -5,  -5,   0,   0,  -5,   0,   0,   0,  -5,    0,   0,   0,   0,   0 },
						{ 22,  0,   0,   0,  17,   0,   0,   0,  18,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 23,  0,   0,   0,  17,   0,   0,   0,  18,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 24,  0,   0,   0,   0,   0,   0,   0,   0,  31,   0,   0,   22,   0,   0,   0,   0 },
						{ 25,  0,   0,   0,   0,   0,   0,   0,   0,   0,  32,   0,    0,   0,   0,   0,   0 },
						{ 26,  0,   0,   0,  17,   0,   0,   0,  18,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 27,  0,   0,   0, -14,   0,   0,   0, -14,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 28,  0,   0,   0, -15,   0,   0,   0, -15,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 29,  0,   0,   0,   0,  -8,   0,   0,   0,  -8,   0,   0,   -8,  23,  -8,  -8,   0 },
						{ 30,  0,   0,   0,   0, -10,   0,   0,   0, -10,   0,   0,  -10, -10, -10, -10,   0 },
						{ 31,  0,   0,   0,   0, -13,   0,   0,   0, -13,   0,   0,  -13, -13, -13, -13,   0 },
						{ 32,  0,   0,   5,   6,   0,   0,   7,   0,   0,   0,   0,    0,   0,   0,   0,   0 },
						{ 33,  0,   0,   0,   0,   0,   0,   0,   0,  -7,   0,   0,   22,   0,   0,   0,   0 },
						{ 34,  0,   0,   5,   6,   0,   0,   7,   0,   0,   0,  35,    0,   0,   0,   0,   0 },
						{ 35,  0,  -6,  -6,  -6,   0,   0,  -6,   0,   0,   0,  -6,    0,   0,   0,   0,   0 },
					};
	///  Matriz IrA
	string arrayHeadirA  [10] = { "estado", "libelula'", "libelula", "arreglo_semtencia", "sentencia", "condicion", "expresion", "termino", "factor", "operadores" };

	int arrayirA [36][10] = {
				  //  e   L'  L   AS  S   C   E   T   F   O	
				   {  0,  0,  1,  0,  0,  0,  0,  0,  0,  0 },
				   {  1,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   {  2,  0,  0,  3,  4,  0,  0,  0,  0,  0 },
				   {  3,  0,  0,  0,  9,  0,  0,  0,  0,  0 },
				   {  4,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   {  5,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   {  6,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   {  7,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   {  8,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   {  9,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 10,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 11,  0,  0,  0,  0,  0, 14, 15, 16,  0 },
				   { 12,  0,  0,  0,  0, 19, 20, 15, 16,  0 },
				   { 13,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 14,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 15,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 16,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 17,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 18,  0,  0,  0,  0,  0, 24, 15, 16,  0 },
				   { 19,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 20,  0,  0,  0,  0,  0,  0,  0,  0, 26 },
				   { 21,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 22,  0,  0,  0,  0,  0,  0, 29, 16,  0 },
				   { 23,  0,  0,  0,  0,  0,  0,  0, 30,  0 },
				   { 24,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 25,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 26,  0,  0,  0,  0,  0, 33, 15, 16,  0 },
				   { 27,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 28,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 29,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 30,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 31,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 32,  0,  0, 34,  4,  0,  0,  0,  0,  0 },
				   { 33,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				   { 34,  0,  0,  0,  9,  0,  0,  0,  0,  0 },
				   { 35,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
				};

	cout<<"______________________________________________________________"<<endl;
	cout<<"\t\t\t Analizador Lexico \t\t\t"<<endl<<endl;	
	arregloCodigo = libelula();
	cout<<"______________________________________________________________"<<endl;
	cout<<"\t\t\t Analizador Sintactico \t\t\t"<<endl<<endl;	

	accion(arregloCodigo, pila, accionHeader, arrayAccion, arrayHeadirA, arrayirA);
	if(pila->dato== 0){
			cout<<"Gramatica no valida..."<<endl;
		}
	return 0;
}	
